//
//  main.m
//  Ospa
//
//  Created by Leandro Nunes on 3/30/14.
//  Copyright (c) 2014 Leandro Nunes. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OSPAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([OSPAAppDelegate class]));
    }
}
