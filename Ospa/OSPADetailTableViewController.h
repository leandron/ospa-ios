//
//  OSPADetailTableViewController.h
//  Ospa
//
//  Created by Leandro Nunes on 3/31/14.
//  Copyright (c) 2014 Leandro Nunes. All rights reserved.
//

#import "OSPAPresentation.h"
#import <UIKit/UIKit.h>

@interface OSPADetailTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UILabel *lblHorario;
@property (weak, nonatomic) IBOutlet UILabel *lblLocal;
@property (weak, nonatomic) IBOutlet UILabel *lblIngresso;
@property (weak, nonatomic) IBOutlet UITextView *txtPrograma;
@property (weak, nonatomic) IBOutlet UILabel *lblTitulo;
@property (weak, nonatomic) OSPAPresentation *presentation;
@end
