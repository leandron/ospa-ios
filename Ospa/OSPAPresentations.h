//
//  OSPAPresentations.h
//  Ospa
//
//  Created by Leandro Nunes on 3/30/14.
//  Copyright (c) 2014 Leandro Nunes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OSPAPresentations : NSObject

+(NSArray *) getAllPresentations;

@end
