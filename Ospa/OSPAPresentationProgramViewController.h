//
//  OSPAPresentationProgramViewController.h
//  Ospa
//
//  Created by Leandro Nunes on 3/31/14.
//  Copyright (c) 2014 Leandro Nunes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OSPAPresentationProgramViewController : UIViewController
@property (weak, nonatomic) NSString *strPrograma;

@end
