//
//  OSPAPresentations.m
//  Ospa
//
//  Created by Leandro Nunes on 3/30/14.
//  Copyright (c) 2014 Leandro Nunes. All rights reserved.
//

#import "OSPAPresentations.h"
#import "OSPAPresentation.h"

@implementation OSPAPresentations

static NSArray *allPresentations;

+(NSArray *) getAllPresentations
{
    if (!allPresentations) {
        NSMutableArray *tmpPresentations = [[NSMutableArray alloc] init];
        NSDictionary *presentationsDict = [self getDictionaryFromJSON];
        
        for (NSDictionary * presentation in presentationsDict)
        {
            NSLog(@"title = %@", [presentation objectForKey:@"nome_evento"]);
            OSPAPresentation *p = [[OSPAPresentation alloc] init];
            p.title = [presentation objectForKey:@"nome_evento"];
            p.tickets = [presentation objectForKey:@"ingressos"];
            p.identifier = [presentation objectForKey:@"id"];
            p.program = [presentation objectForKey:@"programa"];
            p.address = [presentation objectForKey:@"endereco"];
            p.local_name = [presentation objectForKey:@"local"];
            p.date = [presentation objectForKey:@"data"];
            p.time = [presentation objectForKey:@"hora"];
            p.date_time_str = [presentation objectForKey:@"inicio"];
            
            [tmpPresentations addObject:p];
        }
        
        allPresentations = [tmpPresentations copy];
    }
    
    return allPresentations;
}

+ (NSDictionary *) getDictionaryFromJSON
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"items" ofType:@"json"];
    NSString *myJSON = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:[myJSON dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    
    return jsonDictionary;
}

@end
