//
//  OSPAPresentationProgramViewController.m
//  Ospa
//
//  Created by Leandro Nunes on 3/31/14.
//  Copyright (c) 2014 Leandro Nunes. All rights reserved.
//

#import "OSPAPresentationProgramViewController.h"

@interface OSPAPresentationProgramViewController ()

@end

@implementation OSPAPresentationProgramViewController {
    
    __weak IBOutlet UITextView *txtPrograma;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //NSFont *font = [NSFont fontWithName:@"Palatino-Roman" size:14.0];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[self.strPrograma dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    UIFont *font=[UIFont fontWithName:@"Helvetica" size:16.0f];
    //[attributedString setValue:[UIFont fontWithName:@"Helvetica" size:26] forKey:NSFontAttributeName];
    [attributedString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, [attributedString length])];
    txtPrograma.attributedText = attributedString;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)pressBackToPresentationDetails:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
