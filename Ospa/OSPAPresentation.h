//
//  OSPAPresentation.h
//  Ospa
//
//  Created by Leandro Nunes on 3/30/14.
//  Copyright (c) 2014 Leandro Nunes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OSPAPresentation : NSObject

@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *local_name;
@property (strong, nonatomic) NSString *tickets;
@property (strong, nonatomic) NSString *time;
@property (strong, nonatomic) NSString *program;
@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic) NSString *date_time_str;

@end
