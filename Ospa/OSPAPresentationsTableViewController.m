//
//  OSPAPresentationsTableViewController.m
//  Ospa
//
//  Created by Leandro Nunes on 3/30/14.
//  Copyright (c) 2014 Leandro Nunes. All rights reserved.
//

#import "OSPAPresentationsTableViewController.h"
#import "OSPADetailTableViewController.h"
#import "OSPAPresentations.h"
#import "OSPAPresentation.h"

@interface OSPAPresentationsTableViewController ()

@end

@implementation OSPAPresentationsTableViewController

NSArray *allPresentations;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.clearsSelectionOnViewWillAppear = NO;
    
    allPresentations = [OSPAPresentations getAllPresentations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [allPresentations count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PresentationCell" forIndexPath:indexPath];
    
    OSPAPresentation *presentation = [allPresentations objectAtIndex:indexPath.row];
    UILabel *date = (UILabel *)[cell viewWithTag:1];
    UILabel *time = (UILabel *)[cell viewWithTag:2];
    UILabel *title = (UILabel *)[cell viewWithTag:3];
    UILabel *local = (UILabel *)[cell viewWithTag:4];
    date.text = [presentation date];
    time.text = [presentation time];
    title.text = [presentation title];
    local.text = [presentation local_name];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Assume self.view is the table view
    NSIndexPath *path = [self.tableView indexPathForSelectedRow];
    OSPAPresentation *currentPresentation = [allPresentations objectAtIndex:path.row];
    OSPADetailTableViewController *detailView = (OSPADetailTableViewController *)segue.destinationViewController;
    detailView.presentation = currentPresentation;
}



@end
